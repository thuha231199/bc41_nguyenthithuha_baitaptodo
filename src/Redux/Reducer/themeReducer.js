import { DarkTheme } from "../../Styled_Component_ToDoList/ToDoListThemes/DarkTheme";
import { change_theme } from "../types/ToDoListType";
import { arrTheme } from "../../Styled_Component_ToDoList/ToDoListThemes/ThemeManger";
let initialState = {
    themeState : DarkTheme  , 
    
}

export const themeReducer = (state = initialState ,action) => {
    switch (action.type) {
        case change_theme : {
            let theme = arrTheme.find(theme => theme.id === action.themeID) ;
            if (theme) {
                state.themeState = theme.theme ; 
                return {...state} 
            }
        }
        default : 
        return {...state} ; 
    }
}