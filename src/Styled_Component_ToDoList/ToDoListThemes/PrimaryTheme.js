export const PrimaryTheme = {
    bgColor : '#FFFFFF' ,
    color : '#343a40' , 
    borderButton : '1px solid #343a40' , 
    borderRadiusButton : 'none' ,
    hoverTextColor : '#FFFFFF' , 
    hoverBgColor : '#343a40' , 
    borderColor : '#343a40'
}