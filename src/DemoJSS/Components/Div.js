import styled from "styled-components"  ;
import React from "react";

export const Div = ({className , children , ...restProps}) => (
    <div className={className}>
        {children}
    </div>
)

export const DivStyled = styled(Div)`
    padding : 100px ; 
    color : ${props => props.theme.color} ; 
    background : ${props => props.theme.background} ; 
    text-align : ${props => props.theme.textAlign} ; 
`

export const Para = styled.p`
    color : ${props => props.color} ; 
`