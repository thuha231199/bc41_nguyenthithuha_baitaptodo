import { combineReducers } from "redux";
import { themeReducer } from "./themeReducer";
import {toDoListReducer} from "./toDoListReducer";
export const rootReducer = combineReducers({
    themeReducer , 
    toDoListReducer , 
    
})