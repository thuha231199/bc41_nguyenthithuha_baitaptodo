import React, { Component } from 'react';
import { Button } from '../Components/Button';
import { Link, LinkStyled } from '../Components/Link';

class DemoJSS extends Component {
    state = {
        color : 'red' ,
    }
    handleChangeColor = () => {
        this.setState({color : this.state.color === 'blue' ? 'red' : 'blue'})
    }
    render() {
        return (
            <div>
                <Button onClick={this.handleChangeColor} primary = {this.state.color}>Click me!!!</Button>
                <Link className= "text-danger">Chưa có style</Link>
                <LinkStyled id = "123">Đã có styled</LinkStyled>
            </div>
        );
    }
}

export default DemoJSS;