import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import { Container } from '../ComponentsToDoList/Container';
import { Dropdown } from '../ComponentsToDoList/Dropdown';
import { Heading3 } from '../ComponentsToDoList/Heading';
import { TextField } from '../ComponentsToDoList/TextField';
import { Button } from '../ComponentsToDoList/Button';
import { Table, Th, Thead, Tr } from '../ComponentsToDoList/Table';
import { connect } from 'react-redux';
import { AddTaskAction, ChangeTheme, DeleteTaskAction, DoneTaskAction, EditTaskAction, UpdateTaskAction } from '../../Redux/actions/ToDoListAction';
import { arrTheme } from '../ToDoListThemes/ThemeManger';
class ToDoList extends Component {
    state = {
        taskName : '' ,
        id : '' 
    }
    renderTaskToDo = () => {
        return this.props.taskList.filter(task => !task.done).map((task , index)=> {
            return (
                <Tr key={index}>
                <Th>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.setState({taskName : task.taskName , id : task.id})
                        
                    }} className='ml-1'><i class="fas fa-edit"></i></Button>
                    <Button onClick={() => {
                        this.props.dispatch(DoneTaskAction(task.id))
                    }} className='ml-1'><i class="fa fa-check" aria-hidden="true"></i></Button>
                    <Button onClick={() => {
                        this.props.dispatch(DeleteTaskAction(task.id))
                    }} className='ml-1'><i class="fa fa-trash" aria-hidden="true"></i></Button>
                </Th>
            </Tr>
            )
        })
    }
    renderCompletedTask = () => {
        return this.props.taskList.filter(task => task.done).map((task , index)=> {
            return (
                <Tr key = {index}>
                <Th>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.props.dispatch(DeleteTaskAction(task.id))
                    }} className='ml-1'><i class="fa fa-trash" aria-hidden="true"></i></Button>
                </Th>
            </Tr>
            )
        })
    }
    renderTheme = () => {
        return arrTheme.map((theme , index)=> {
            return <option key={index} value ={theme.id}>{theme.name}</option>
        })
    }
    render() {
        
        return (
            <ThemeProvider theme={this.props.theme}>
                <Container className='w-50'>
                    <Dropdown onChange={(e) => {
                        let value = e.target.value;
                        this.props.dispatch(ChangeTheme(value)) 
                    }}>
                        {this.renderTheme()}
                    </Dropdown>
                    <Heading3>To Do List</Heading3>
                    <TextField value = {this.state.taskName} onChange = {(e) => {
                        this.setState({taskName : e.target.value})
                    }} name = "taskName" label='task name'  />
                    <Button onClick={() => {
                        let {taskName} = this.state ; 
                        let newTask = {
                            id : Date.now(),
                            taskName : taskName , 
                            done : false
                        } ; 
                        this.props.dispatch(AddTaskAction(newTask))
                        this.setState({taskName : ""})
                    }} className='ml-2'><i class="fa fa-plus" aria-hidden="true"></i> Add task</Button>
                    <Button onClick={() => {
                        let {taskName , id} = this.state ; 
                        let updatedTask = {
                            taskName , 
                            id 
                        }
                        this.props.dispatch(UpdateTaskAction(updatedTask))
                        this.setState({taskName : "" , id : ""})
                    }} className='ml-2'><i class="fas fa-file-upload"></i> Update task</Button>
                    <hr/>
                    <Heading3>Task to do</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskToDo()}
                        </Thead>
                    </Table>
                    <Heading3>Task completed</Heading3>
                    <Table>
                        <Thead>
                            {this.renderCompletedTask()}
                        </Thead>
                    </Table>
                </Container>
            </ThemeProvider>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        theme : state.themeReducer.themeState ,
        taskList : state.toDoListReducer.taskList , 
        // taskEdit : state.toDoListReducer.taskEdit , 
    }
}


export default connect(mapStateToProps)(ToDoList);