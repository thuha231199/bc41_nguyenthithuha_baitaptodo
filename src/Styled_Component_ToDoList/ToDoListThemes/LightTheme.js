export const LightTheme = {
    bgColor : '#FFFFFF' ,
    color : '#7952b3' , 
    borderButton : '1px solid #7952b3' , 
    borderRadiusButton : 'none' ,
    hoverTextColor : '#FFF' , 
    hoverBgColor : 'rgb(121, 82, 179 , 0.5)' , 
    borderColor : '#7952b3' , 
}