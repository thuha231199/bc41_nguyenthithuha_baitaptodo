import styled from "styled-components";

export const Button = styled.button`
    padding: 10px;
    color : white ; 
    background-color : ${props => props.primary} ; 
    font-size : 20px ; 
    border : none ; 
    cursor : pointer ; 
    opacity : 1 ; 
    transition : .4s linear ; 
    border-radius : 10px ; 
    &:hover {
        opacity : .5 ; 

    }
`