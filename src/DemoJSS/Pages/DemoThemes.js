import React from 'react'
import { ThemeProvider } from 'styled-components'
import { DivStyled, Para } from '../Components/Div' 
import { useState } from 'react';

function DemoThemes() {
    const DarkTheme = {
        color : 'pink' , 
        background : 'black' , 
        textAlign : 'center' ,
    }
    const LightTheme = {
        color : 'black' , 
        background : 'gray' , 
        textAlign : 'left'  ,
    }
    const [theme, settheme] = useState(DarkTheme) ; 
    let handleChangeMode = (e) => {
        settheme(e.target.value === "1" ? DarkTheme : LightTheme) ; 
    }
  return (
    <ThemeProvider theme={theme}>
        <DivStyled>Hello FE!!!</DivStyled>
        <select onChange={handleChangeMode}>
            <option value="1">Default Mode</option>
            <option value="1">Dark Mode</option>
            <option value="2">Light Mode</option>
        </select>
        <Para color={theme.color}>Hello BE . Sau này có dịp chúng ta sẽ gặp nhau!!!</Para>
    </ThemeProvider>
  )
}

export default DemoThemes