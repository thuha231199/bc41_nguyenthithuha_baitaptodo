import { add_task, change_theme, delete_task, done_task, edit_task, update_task } from "../types/ToDoListType";

export const AddTaskAction = (newTask) => {
    return {
        type: add_task,
        newTask
    }
}

export const ChangeTheme = (value) => {
    return {
        type : change_theme , 
        themeID : value 
    }
}

export const DoneTaskAction = (id) => {
    return {
        type : done_task , 
        id 
    }
}

export const DeleteTaskAction = (id) => {
    return {
        type : delete_task , 
        id
    }
}

// export const EditTaskAction = (task) => {
//     return {
//         type : edit_task , 
//         task : task 
//     }
// }

export const UpdateTaskAction = (updatedTask) => {
    return {
        type : update_task , 
        updatedTask
    }
}
