import { add_task, delete_task, done_task, edit_task, update_task } from "../types/ToDoListType"

const initialState = {
    taskList : [] ,
}

export const toDoListReducer =  (state = initialState, action) => {
  switch (action.type) {
    case add_task : {
      if (action.newTask.taskName.trim() === "") {
        alert('Task is required!')
        return {...state }
      }
      let taskListClone = [...state.taskList] ; 
      let index = taskListClone.findIndex(task => task.taskName === action.newTask.taskName) ; 
      if (index !== -1) {
        alert('Task already exists') ; 
        return {...state} ; 
      }
      taskListClone.push(action.newTask) ; 
      state.taskList = taskListClone ; 
      return {...state } ;
    }
    case done_task : {
      let taskListClone = [...state.taskList] ; 
      let index = taskListClone.findIndex(task => task.id === action.id) ; 
      taskListClone[index].done = true ; 
      return {...state , taskList : taskListClone} ; 
    }
    case delete_task : {
      let taskListClone = [...state.taskList] ; 
      let filteredTask = taskListClone.filter(task => task.id !== action.id) ; 
      return {...state , taskList : filteredTask} ; 
    }
    // case edit_task : {
    //   state.taskEdit = action.task ; 
    //   return {...state} ; 
    // }
    case update_task : {
      let taskListClone = [...state.taskList] ; 
      let index = taskListClone.findIndex(task => task.id === action.updatedTask.id) ; 
      taskListClone[index].taskName = action.updatedTask.taskName ; 
      state.taskList = taskListClone ; 
      return {...state} ; 
    }


  default:
    return {...state}
  }
}
