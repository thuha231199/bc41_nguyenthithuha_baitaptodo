import styled from "styled-components"
import React from "react" ; 
export let Link = ({className ,  id , children ,  ...restProps}) => 
    (<a className = {className} id = {id}>
        {children}
    </a> )


export const LinkStyled = styled(Link) `
    color : blue !important ; 
    font-size : 30px ; 
    font-weight : bold ; 
`
