// import DemoJSS from "./DemoJSS/Pages/DemoJSS";
// import DemoThemes from "./DemoJSS/Pages/DemoThemes";

import ToDoList from "./Styled_Component_ToDoList/ToDoList/ToDoList";


function App() {
  return (
    <div>
      {/* <DemoJSS/> */}
      {/* <DemoThemes/> */}
      <ToDoList/>
    </div>
  );
}

export default App;
